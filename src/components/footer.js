import React from 'react';
import {Link} from 'gatsby'

const footer = props => {
    return (
        <footer className="fdb-block footer-large bg-dark" data-block-type="footers" data-id="2">
  <div className="container">
    <div className="row align-items-top text-center text-md-left">
        <div className="col-12 col-sm-6 col-md-4">
            <h3>LN2X Group</h3>
            <p>Strategy, Implementation, Metrics</p>
            <p>Registered: 19 North Street, Ashford, Kent, TN24 8LF, United Kingdom</p>
            <p>LN2X is focused on building practical solutions that help improve managing trust, compliance, risk and security</p>
        </div>

    <div className="col-12 col-sm-6 col-md-4 mt-4 mt-sm-0">
        <h3>Contact</h3>
        <p>Email: info [at] ln2x [dot] io</p>
        <p>Call<br/>[EMEA]+442033229095<br/>[LATAM]+528141707161</p>
    </div>

<div className="col-12 col-md-4 mt-5 mt-md-0 text-md-left">
  <h3>Useful Links</h3>
  <p><Link to="/privacy">Privacy Policy</Link></p>
  <p><Link to="/about">About LN2X</Link></p>
  <p><a href="https://ln2x.com/en/training/elearning/">Full Catalogue</a></p>
  <p><Link to="/contact">Contact</Link></p>
    </div>
    </div>
  <div className="row mt-5">
    <div className="col text-center">
      © 2019 ln2x.io All Rights Reserved
    </div>
  </div>
  </div>
</footer>
    );
};

footer.propTypes = {
    
};

export default footer;