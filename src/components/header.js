import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"



const Header = ({ siteTitle }) => (
  <header className="bg-dark" data-block-type="headers" data-id="1">
      <div className="container">
      <nav className="navbar navbar-expand-md">
        <a className="navbar-brand" href="/">
          <img src="/assets/images/ln2x.io-dark-multicolour.png" alt="image"/>
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav11" aria-controls="navbarNav11" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav11">
          <ul className="navbar-nav ml-auto" role="navigation">
            <Link className="nav-link" to="/">Home</Link>
            <Link className="nav-link" to="/learn">Learn</Link>
            <Link className="nav-link" to="/help">Get Help</Link>
            <Link className="nav-link" to="/control">Control</Link>
            <Link className="nav-link" to="/contact">Contact</Link>
            <li className="nav-item"><a className="nav-link" href="https://calendly.com/ln2x" target="_blank">Lets Talk</a></li>
          </ul>
        </div>
      </nav>
      </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
