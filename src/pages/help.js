import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

class HelpPage extends React.Component{
  componentDidMount() {
    const head = document.querySelector('head');
    const script = document.createElement('script');
    script.setAttribute('src',  'https://assets.calendly.com/assets/external/widget.js');
    head.appendChild(script);
  }
render(){
  return(
  <Layout>
    <SEO title="Help" />
    <section className="fdb-block" data-block-type="features" data-id="22" draggable="false">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-12 text-center">
        <h1>Consulting & Advisory</h1>
        <p className="lead">LN2X has access to over 10000 specialists globally which can be mobilised for any project. Our inhouse expert advisors have a thorough understanding of implementing standards and information security best practices including:</p>
      </div>
    </div>
  </div>
  <div className="container">
    <div className="row text-left mt-5">
      <div className="col-12 col-sm-6 col-lg-3">
        <div className="row">
          <div className="col-3" data-aos="fade-up" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/layers.svg"/>
          </div>
          <div className="col-9" data-aos="fade-down" data-aos-delay="400">
            <h4>Payment Security Standards</h4>
            <p>PCI DSS, PA DSS, PTS, Card Personalisation, EMV, ATM/POS Security, </p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-sm-0">
        <div className="row">
          <div className="col-3" data-aos="fade-left" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/layout.svg"/>
          </div>
          <div className="col-9" data-aos="fade-right" data-aos-delay="400">
            <h4>ISO Standards</h4>
            <p>ISO 27001/2, 22301</p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0">
        <div className="row">
          <div className="col-3" data-aos="fade-right" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/life-buoy.svg"/>
          </div>
          <div className="col-9" data-aos="fade-up" data-aos-delay="400">
            <h4>Infosec Standards/Frameworks</h4>
            <p>Common Controls Framework, CIS, NIST Cybersecurity Framework</p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0">
        <div className="row">
          <div className="col-3" data-aos="fade-down" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/map-pin.svg"/>
          </div>
          <div className="col-9" data-aos="fade-left" data-aos-delay="400">
            <h4>Application Security Lifecycle</h4>
            <p>OWASP, CERT, CWE, ISO/IEC 27034, AGILE, ISO800-53</p>
          </div>
        </div>
      </div>
    </div>

    <div className="row text-left pt-3 pt-lg-5">
      <div className="col-12 col-sm-6 col-lg-3">
        <div className="row">
          <div className="col-3" data-aos="fade-right" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/monitor.svg"/>
          </div>
          <div className="col-9" data-aos="fade-up" data-aos-delay="400">
            <h4>Project & Implementation Management</h4>
            <p>Strategy, Management and Solution Integration.</p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-sm-0">
        <div className="row">
          <div className="col-3" data-aos="fade-down" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/package.svg"/>
          </div>
          <div className="col-9" data-aos="fade-left" data-aos-delay="400">
            <h4>Risk Management</h4>
            <p>ISO 31000, NIST 800-30</p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0">
        <div className="row">
          <div className="col-3" data-aos="fade-right" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/cloud.svg"/>
          </div>
          <div className="col-9" data-aos="fade-up" data-aos-delay="400">
            <h4>Pen Testing</h4>
            <p>OSSTMM, CEH, OPSA, OPST, OSCP</p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0">
        <div className="row">
          <div className="col-3" data-aos="fade-down" data-aos-delay="400">
            <img alt="image" className="img-fluid" src="/assets/icons/gift.svg"/>
          </div>
          <div className="col-9" data-aos="fade-left" data-aos-delay="400">
            <h4>Privacy</h4>
            <p>GDPR, Privacy Directives, DLP</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section className="fdb-block" data-block-type="features" data-id="22" draggable="false">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-12 text-center" data-aos="fade-left" data-aos-delay="200">
        <h1>Why LN2X?</h1>
        <p>LN2X provides specialised consulting and advisory services stemming from our deep routed understanding of payment systems, cloud systems, security and compliance. We provide an independent perspective on the security posture of an organization and help maintain a system of checks and balances with in-house personnel. Outsourcing to us may provide an integrated, more coherent solution, thereby eliminating redundant effort, hardware, and software. Our services are governed by the following principles and working practices</p>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <h2>Objectivity</h2>
        <p>An organization may have multiple, ad hoc solutions to handle the same types of security problems. There may be no enterprise-wide management of security or of strategy. Moving security to a capable security service provider may help simplify and strengthen the enterprise's security posture. We provide an independent perspective on the security posture of an organization and help maintain a system of checks and balances with in-house personnel. Outsourcing to us may provide an integrated, more coherent solution, thereby eliminating redundant effort, hardware, and software.</p>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" height="250" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_Data_points_ubvs.png"/>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_Security_on_s9ym.png"/>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <h2>Effectivity</h2>
        <p>Service security solutions and technologies, in general, are far more effective because they are managed and monitored by skilled security professionals. For example, when an intrusion is detected, we use a remote monitoring connection to determine whether the alarm is justified and block further intruder actions. A managed service can protect the client’s network from unsecured VPN endpoints. On occasions we may use other third party provider products as the basis for providing service. We have worked with hundreds and providers from excellent to extremely poor, we bring that experience to the table, ensuring we only provide best-in-className solutions and can customise them to meet your specific requirements.</p>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <h2>Performance</h2>
        <p>When an organization contracts for security monitoring services, the service can report near real-time results, 24 hours a day, 7 days a week, and 365 days a year. This is a large contrast with an in-house service that may only operate during normal business hours. As a provider we can be held accountable for the service standards we provide. We guarantee service levels and assure our availability; failing to do so can have financial repercussions. Our operational procedures are designed to ensure uninterrupted service availability. If we are providing service systems, then it is our responsibility to upgrade software and hardware and to maintain a secure network configuration. Because we have strict contractual obligations to our clients and must maintain our reputation in the marketplace, our control procedures are generally both well documented and carefully enforced.</p>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_analytics_5pgy.png"/>
      </div>

    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_creative_team_r90h.png"/>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <h2>Staffing</h2>
        <p>A shortage of qualified information security personnel puts tremendous pressure on IT departments to recruit, train, compensate, and retain critical staff . The cost of in-house network security specialists can be prohibitive. When outsourcing, the costs to hire, train, and retain highly skilled staff becomes our responsibility. We retain security experts by offering a range of career opportunities and positions from entry level to senior management, all within the information security field. In addition, if a client organization can outsource repetitive security monitoring and protection functions, then they can then focus internal resources on more critical business initiatives.</p>

      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <h2>Risk</h2>
        <p>Whilst we may have more competent staff in certain domains, they may not always be as effective in applying remedies that meet your needs, this is why we ensure that any resources assigned understand your business as well as you do. We customise generic solutions to meet your needsAlso, sometimes the client’s staff is more adept at providing the best solution. When engaging on our partnership we approach it understanding our responsibility and treating your challenges as our own always. We understand that regardless of what we take on there are risks to your business and do our utmost to advise on how to address the impact of risks to your business and taking prompt action. Your risk is our shared responsibility and our aim is always to go out of our way to help you.</p>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_vault_9cmw.png"/>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_safe_bnk7.png"/>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <h2>Trust</h2>
        <p>The challenge of establishing a good working relationship and building trust hinges on us proving our capability at all times. As your trusted service provider we may have access to sensitive information and details about your security posture and vulnerabilities, which we take very seriously. Everything we predicate, we practice;this means that we treat your confidential data as if it was our own, we also ensure that only those with an absolute need to access your data can do so. Access to your systems and data are controlled and logged at all times. Our systems undergo rigorous testing on a regular basis adhering to the strictest security standards. Our staff are security vetted and continuously encouraged to deliver quality services with KPIs and CSFs.</p>

      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <h2>Skills</h2>
        <p>An in-house staff member who only deals with security on a part-time basis or only sees a limited number of security incidents is probably not as competent as someone who is doing the same work full-time, seeing security impacts across several different clients, and crafting security solutions with broader applicability. We have insight into security situations based on extensive experience, dealing with hundreds or thousands of potentially threatening situations every day, and are some of the most aggressive and strenuous users of security software.</p></div>
        <div className="col" data-aos="fade-left" data-aos-delay="200">
          <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_operating_system_4lr6.png"/>
        </div>
      </div>
    </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_product_teardown_elol.png"/>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <h2>Awareness</h2>
        <p>It is difficult for an organization to track and address all potential threats and vulnerabilities as well as attack patterns, intruder tools, and current best security practices. We are often able to obtain advance warning of new vulnerabilities and gain early access to information on countermeasures. We can advise on how other organizations handle the same types of security problems. We have contact with highly qualified and specialized international security experts as well as other providers. We use these resources to diagnose and resolve client issues.</p>

      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <h2>Dependence</h2>
        <p>We realise that clients can become operationally dependent on our services and be greatly affected by the our business viability, other clients, and business partnerships. Our contracts and terms ensure that no matter what happens to our business you are always in control of your systems and data. We use Software Escrow Services and Cyber Security Insurance that provides you with peace of mind. Our contracts are not one-sided and we assume the liability and consequences in unlikely event that our service has a detrimental impact to your business due to negligence or miscommunication or any failure in our ability to deliver services as agreed.</p>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_status_update_jjgk.png"/>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_done_a34v.png"/>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <h2>Ownership</h2>
        <p>You retain ownership and responsibility for your operation and the protection of its critical assets regardless of the scope of services provided by a rovider, we are well aware of this. Our promise: We will never ignore pressing security issues due to “out of sight,out of mind” thinking, having delegated this concern to the provider. The client must ensure that it retains sufficient competency to fulfill its responsibility and that contractual and service level agreement language supports this. Risk mitigation approaches include making information security the primary responsibility for one or more staff members and managers and conducting regular user security awareness and training sessions.</p>

      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <h2>Implementation</h2>
        <p>Starting a service relationship may require a complex transition of people, processes, hardware, software, and other assets or migration from your existing provider/solution, all of which may introduce new risks. IT and business environments may require new interfaces, approaches, and expectations for service delivery. We analyse these requirements in detail prior to starting so both parties understand the implications, timescales and consequences. We work with you to ensure that we achieve your objectives without impacting business operations.</p>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_laravel_and_vue_59tp.png"/>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="contents" data-id="10" draggable="false">
  <div className="container">
    <div className="row align-items-center">
      <div className="col" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid rounded-0" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_pie_chart_6efe.png"/>
      </div>
      <div className="col" data-aos="fade-left" data-aos-delay="200">
        <h2>Cost</h2>
        <p>The cost is typically less than hiring in-house, full-time security experts. We are able to spread out the investment in analysts, hardware, software, and facilities over several clients, reducing the per client cost. A client organization can convert variable costs (when done in-house) to fixed costs (services), realize a tax advantage by deducting our service fee expenses from current year earnings versus depreciating internal assets, and experience cash flow improvements resulting from the transfer of software licenses (and possibly other resources).</p>

      </div>
    </div>
  </div>
</section>

  </Layout>
  )}
}

export default HelpPage;
