import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const PrivacyPage = () => (
  <Layout>
    <SEO title="About" />
    <section class="fdb-block team-5" data-block-type="teams" data-id="33" draggable="false">
    <div class="container">
        <div class="row text-center justify-content-center">
        <div class="col-8">
            <h1>About LN2X</h1>
        </div>
        </div>
        <div class="row-70"></div>
        <div class="row text-left pt-4">
        <div class="col-12 col-md-6">
            <p class="lead">LN2X is the sister company of LiquidNexxus, a company with considerable track record in training management. We started LN2X because we wanted to create practical innovative solutions. Leveraging our network and experience in the industry we seek to bring sanity to a market dominated by expensive/complex solutions in order to drive best practices.</p>
        </div>
        <div class="col-12 col-md-6">
            <p class="lead">Our vision is to create practical solutions to help organisations tackle challenges in managing governance, risk and compliance. Our solutions are designed to be lightning fast, easy to use and accesible to SME's and corporates. We believe there is a better way, the LN2X way. LN2X has worked with organisations of all sizes, here is a selection</p>
        </div>
        </div>
        </div>
    </section>

    <div className="container">
    <section class="fdb-block team-5" data-block-type="teams" data-id="33" draggable="false">
    <div class="row text-center justify-content-center" data-aos="fade-left" data-aos-delay="200">
      <div class="col-sm-2 m-sm-auto">
        <img alt="image" class="img-fluid" src="/assets/images/hsbc-logo.png"/>
      </div>
      <div class="col-sm-2 m-sm-auto">
        <img alt="image" class="img-fluid" src="/assets/images/2000px-ABSA_logo.svg.png"/>
      </div>
      <div class="col-sm-2 m-sm-auto">
        <img alt="image" class="img-fluid" src="/assets/images/Banelco_logo_2013.png"/>
      </div>
      <div class="col-sm-2 m-sm-auto ">
        <img alt="image" class="img-fluid" src="/assets/images/Shell_logo.svg.png"/>
      </div>
    </div>
    <div class="row text-center justify-content-center" data-aos="fade-right" data-aos-delay="400">
      <div class="col-sm-2 m-sm-auto">
        <img alt="image" class="img-fluid" src="/assets/images/santander-bank-logo.png"/>
      </div>
      <div class="col-sm-2 m-sm-auto">
        <img alt="image" class="img-fluid" src="/assets/images/Banamex-logo.png"/>
      </div>
      <div class="col-sm-2 m-sm-auto">
        <img alt="image" class="img-fluid" src="/assets/images/Banco_de_Chile_Logo.png"/>
      </div>
      <div class="col-sm-2 m-sm-auto ">
        <img alt="image" class="img-fluid" src="/assets/images/Scotiabank-Logo-PNG-03791-1.png"/>
      </div>
   </div>
   <div class="row text-center justify-content-center" data-aos="fade-left" data-aos-delay="800">
     <div class="col-sm-2 m-sm-auto">
       <img alt="image" class="img-fluid" src="/assets/images/BBVA_logo(2).jpg"/>
     </div>
     <div class="col-sm-2 m-sm-auto">
       <img alt="image" class="img-fluid" src="/assets/images/Davivienda-Logo-EPS-vector-image.png"/>
     </div>
     <div class="col-sm-2 m-sm-auto">
       <img alt="image" class="img-fluid" src="/assets/images/EUROPOL_logo.svg.png"/>
     </div>
     <div class="col-sm-2 m-sm-auto ">
       <img alt="image" class="img-fluid" src="/assets/images/First_National_Bank_Logo.png"/>
     </div>
   </div>
   <div class="row text-center justify-content-center" data-aos="fade-right" data-aos-delay="1000">
     <div class="col-sm-2 m-sm-auto">
       <img alt="image" class="img-fluid" src="/assets/images/first-data-logo.png"/>
     </div>
     <div class="col-sm-2 m-sm-auto">
       <img alt="image" class="img-fluid" src="/assets/images/VerifoneLogo.png"/>
     </div>
     <div class="col-sm-2 m-sm-auto">
       <img alt="image" class="img-fluid" src="/assets/images/Pick-n-Pay-logo.png"/>
     </div>
     <div class="col-sm-2 m-sm-auto ">
       <img alt="image" class="img-fluid" src="/assets/images/Redbancvectorlogo.png"/>
     </div>
   </div>
</section>
</div>
<section class="fdb-block" data-block-type="features" data-id="24" draggable="false">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <h1>Schedule a Call</h1>
        <p>The best things start with a discussion, we would love to hear from you.</p>
        <p><a class="btn btn-primary mt-4 mb-5 mb-md-0" href="https://calendly.com/ln2x" target="_blank">Lets Arrange a Meeting</a></p>
      </div>
    </div>
  </div>
</section>
    
  </Layout>
)

export default PrivacyPage;
