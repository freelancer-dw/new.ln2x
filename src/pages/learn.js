import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const LearnPage = () => (
  <Layout>
    <SEO title="Learn" />
    <section class="fdb-block" data-block-type="call_to_action" data-id="16" draggable="false">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col">
            <h1 align="center">Training</h1>
            <p class="lead" align="center">We understand training & awareness, especially in complex environments with a multitude of requirements. The LN2X team have delivered face-to-face and eLearning to over 7000 professionals across over 50 countries. We've used and built technical learning content and learning management solutions, and we actively works with some of the worlds largest organisations advising on Learning & Development. Coming soon a completely new LMS module which works with our GRC solution.</p>
          </div>
        </div>
      </div>
    </section>
    <section className="fdb-block" data-block-type="features" data-id="22" draggable="true">
    <div className="container">
      <div className="row text-center mt-5">
        <div className="col-12 col-sm-4" data-aos="fade-right" data-aos-delay="200">
          <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_authentication_fsn5.png"/>
          <h3><strong>Intuitive</strong></h3>
          <p>"Simple interface with clear options. Open browser, log in and start learning "</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0"  data-aos="fade-left" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="55%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_JavaScript_Frameworks_x21l.png"/>
          <h3><strong>Lightning Fast</strong></h3>
          <p>Built using actual modern technology, HTML5, CSS, JS, LRS</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-down" data-aos-delay="200">
          <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_creation_process_vndn.png"/>
          <h3><strong>Interactive Content</strong></h3>
          <p>Built-in content interaction, rewards, assesments, certificate downloads, notifications</p>
        </div>
      </div>
      <div className="row text-center mt-5">
        <div className="col-12 col-sm-4" data-aos="fade-right" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="55%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_control_panel1_20gm.png"/>
          <h3><strong>Custom Branding & DNS</strong></h3>
          <p>"Customise layouts, themes, icons, images, logos, favicons and, yes, your own domain and sender identity"</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-up" data-aos-delay="200">
          <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_features_overview_jg7a.png"/>
          <h3><strong>Ready Made Catalogue</strong></h3>
          <p>Over 500 courses from compliance and standards to encryption and application security</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-left" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="55%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_marketing_v0iu.png"/>
          <h3><strong>In-built Support </strong></h3>
          <p>Our support team is ready to help with setup, design, configuration or even complete custom projects.</p>
        </div>
      </div>

    <div className="row text-center mt-5">
      <div className="col-12 col-sm-4" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid" width="65%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_grades_j6jn.png"/>
        <h3><strong>Advanced Analytics & Reporting</strong></h3>
        <p>"Report on progress down to interaction/question/answer level, easily create benchline KPIs and CSFs"</p>
      </div>

      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_browser_stats_704t.png"/>
        <h3><strong>Segmentation, Groups & Cohorts</strong></h3>
        <p>Easily create groups and manage learning paths. Configure suggestions based on user preferences and interests</p>
      </div>

      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_organize_resume_utk5.png"/>
        <h3><strong>Policy Acknowledgement</strong></h3>
        <p>Include your own policies, assign to users for completion. Easily comply with all PCI DSS confirmation requirements; infosec policy signoff, key custodian acknowledgement...</p>

      </div>
    </div>
    </div>
  </section>

  <section className="fdb-block" data-block-type="features" data-id="24" draggable="false">
      <div className="container">
        <div className="row text-center">
        <div className="col-4">
        <h2>Login</h2>
        <p className="lead">Already registered? Login here</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="https://learn.ln2x.io" target="_blank">Login</a></p>
        </div>
        <div className="col-4">
        <h2>Demo</h2>
        <p className="lead">contact us to arrange a demo.</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="https://calendly.com/ln2x" target="_blank">Schedule a Call</a></p>
        </div>
        <div className="col-4">
        <h2>Catalogue</h2>
        <p className="lead">Browse our full eLearning Catalogue</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="https://ln2x.com/en/training/elearning/" target="_blank">Course Catalogue</a></p>
        </div>

      </div>
    </div>
  </section>

  <section class="fdb-block" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@2.0.1/dist/imgs//shapes/8.svg);" data-block-type="pricings" data-id="29" draggable="true">
    <div class="container">
      <div class="row text-center">
        <div class="col">
          <h1>Pricing Plans</h1>
        </div>
      </div>
      <div class="row mt-5 align-items-center no-gutters">
        <div class="col-12 col-sm-10 col-md-8 m-auto col-lg-4 text-center shadow">
          <div class="bg-white pb-5 pt-5 pl-4 pr-4 rounded-left">
            <h2 class="font-weight-light">Basic</h2>
            <p class="h1 mt-5 mb-5"><strong>from $39</strong> <span class="h4">/user per year</span></p>
            <ul class="text-left">
              <li>Custom Branded</li>
              <li>One Advanced PCI Awareness Course</li>
              <li>Prebuilt Reports & Advanced Analytics</li>
              <li>Annual Discount Available</li>
            </ul>
            <p class="text-center pt-4"><a href="https://calendly.com/ln2x" class="btn btn-outline-dark">Discuss this Package</a></p>
          </div>
        </div>
        <div class="col-12 col-sm-10 col-md-8 ml-auto mr-auto col-lg-4 text-center mt-4 mt-lg-0 sl-1 pt-0 pt-lg-3 pb-0 pb-lg-3 bg-white fdb-touch rounded shadow">
          <div class="pb-5 pt-5 pl-4 pr-4">
            <h2 class="font-weight-light">Standard</h2>
            <p class="h1 mt-5 mb-5"><strong>from $59</strong> <span class="h4">/month</span></p>
            <ul class="text-left">
              <li>All Basic Items</li>
              <li>Dedicated L&D/Account Manager</li>
              <li>"Ask the Expert" Security/Compliance Support </li>
            </ul>
            <p class="text-center pt-4"><a href="https://calendly.com/ln2x" class="btn btn-primary btn-shadow">Discuss this Package</a></p>
          </div>
        </div>
        <div class="col-12 col-sm-10 col-md-8 ml-auto mr-auto col-lg-4 text-center mt-4 mt-lg-0 shadow">
          <div class="bg-white pb-5 pt-5 pl-4 pr-4 rounded-right">
            <h2 class="font-weight-light">Enterprise</h2>

            <p class="h1 mt-5 mb-5"><strong>from $99</strong> <span class="h4">/month</span></p>

            <ul class="text-left">
              <li>All Standard Items</li>
              <li>4 Hours/Month Dedicated Security/Compliance Consultant</li>
              <li>Access to early release GRC Modules</li>
            </ul>
            <p class="text-center pt-4"><a href="https://calendly.com/ln2x" class="btn btn-outline-dark">Discuss this Package</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>


  
  </Layout>
)

export default LearnPage;
