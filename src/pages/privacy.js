import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const PrivacyPage = () => (
  <Layout>
    <SEO title="Privacy Policy" />
    <section class="fdb-block" data-block-type="contents" data-id="5">
 <div class="container">
  <div class="row">
    <div class="col text-center">
<h1>Privacy Policy</h1>
    </div>
 </div>
 </div>
</section>

<section>
  <div class="container">
   <div class="row">
        <h3>BACKGROUND</h3>
        <p>LiquidNexxus Limited understands that your privacy is important to you and that you care about how your personal data is used. We respect and value the privacy of everyone who visits this website, ln2x.com and ln2x.io (“LN2X”) and will only collect and use personal data in ways that are described here, and in a way that is consistent with our obligations and your rights under the law.</p>
        <p>Please read this Privacy Policy carefully and ensure that you understand it. You will be required to read and accept this Privacy Policy when signing up for an Account. If you do not accept and agree with this Privacy Policy, you must stop using LN2X Services immediately.</p>

        <h3>Definitions and Interpretation</h3>
<p>In this Policy the following terms shall have the following meanings:
<br/>r>“Account” means an account required to access and/or use certain areas and features of LN2X;
<br/>r>“Cookie”means a small text file placed on your computer or device by LN2X when you visit certain parts of LN2X and/or when you use certain features of LN2X. Details of the Cookies used by LN2X are set out in Part 14, below; and“Cookie Law”means the relevant parts of the Privacy and Electronic Communications (EC Directive) Regulations 2003</p>

<h3>Information About us</h3>
<p>LN2X is owned and operated by LN2X Group Ltd and/or LiquidNexxus Limited a company registered in England under company number 6959415.
<br/>Data Protection Officer Email address: dpo@ln2x.io</p>

<h3>What Does This Policy Cover?</h3>
<p>This Privacy Policy applies only to your use of any LN2X service on any ln2x domain including .IO, .COM . LN2X may contain links to other websites. Please note that we have no control over how your data is collected, stored, or used by other websites and we advise you to check the privacy policies of any such websites before providing any data to them.</p>

<h3>What is Personal Data?</h3>
<p>Personal data is defined by the General Data Protection Regulation (EU Regulation 2016/679) (the “GDPR”) as ‘any information relating to an identifiable person who can be directly or indirectly identified in particular by reference to an identifier’.</p>
<p>Personal data is, in simpler terms, any information about you that enables you to be identified. Personal data covers obvious information such as your name and contact details, but it also covers less obvious information such as identification numbers, electronic location data, and other online identifiers.</p>

<h3>What Are My Rights?</h3>
<p>Under the GDPR, you have the following rights, which we will always work to uphold:
<p>The right to be informed about our collection and use of your personal data. This Privacy Policy should tell you everything you need to know, but you can always contact us to find out more or to ask any questions using the details in Part 15.The right to access the personal data we hold about you. Part 13 will tell you how to do this.The right to have your personal data rectified if any of your personal data held by us is inaccurate or incomplete. Please contact us using the details in Part 15 to find out more.The right to be forgotten, i.e. the right to ask us to delete or otherwise dispose of any of your personal data that we have. Please contact us using the details in Part 15 to find out more.
The right to restrict (i.e. prevent) the processing of your personal data.
The right to object to us using your personal data for a particular purpose or purposes.
The right to data portability. This means that, if you have provided personal data to us directly, we are using it with your consent or for the performance of a contract, and that data is processed using automated means, you can ask us for a copy of that personal data to re-use with another service or business in many cases.
Rights relating to automated decision-making and profiling. In order to provide our services we may use some of your personal data to make decisions and to provide recommendations. Use of this data is limited to the scope of the services we offer you and is solely intended for the purposes and objectives thereof. LN2X uses a variety of platforms in order to provide the services in all such usage we ensure your data remains secure and under our control with strong access control measures and multifactor authentication.</p>
<p>For more information about our use of your personal data or exercising your rights as outlined above, please contact us using the details provided in Part 15.</p>
<p>Further information about your rights can also be obtained from the Information Commissioner’s Office or your local Citizens Advice Bureau.</p>
<p>If you have any cause for complaint about our use of your personal data, you have the right to lodge a complaint with the Information Commissioner’s Office.</p>

</p>
<h3>What Data Do we Collect?</h3>
<p>Depending upon your use of LN2X, we may collect some or all of the following personal and non-personal data (please also see Part 14 on our use of Cookies and similar technologies:</p>
<p>
  <br/>If you visit our website from a dynamic IP (mobile phone)
  <br/>IP address;
  <br/>Web browser type and version;
  <br/>Operating system;
  <br/>A list of URLs starting with a referring site, your activity on LN2X, and the site you exit to;
  <br/>If you are visiting from a fixed IP address
  <br/>Business name and IP address;
  <br/>Business Registered Address;;
  <br/>Business Website Address;
  <br/>Business Telephone number;
  <br/>If you provide us information through emails, forms and profile preferences
  <br/>Name;
  <br/>Email address;
  <br/>Telephone number;
  <br/>Job title;
  <br/>Business name;
  <br/>Information about your preferences and interests;
  <br/>Test, Feedback and Survey Results
  <br/>Dietary Preferences
  <br/>Sensitive Company Information such as Invoices and other sensitive documents required for processing services (Banks Details, Credit Cards, Passport Details)

<h3>How Do You Use My Personal Data?</h3>
<p>Under the GDPR, we must always have a lawful basis for using personal data. This may be because the data is necessary for our performance of a contract with you, because you have consented to our use of your personal data, or because it is in our legitimate business interests to use it. Your personal data may be used for the following purposes:</p>
<p><br/>Providing and managing your Account;</p>
<br/>Providing and managing your access to LN2X;
<br/>Personalising and tailoring your experience on LN2X;
<br/>Supplying our services to you. Your personal details are required in order for us to enter into a contract with you.
<br/>Personalising and tailoring our services for you.
<br/>Communicating with you. This may include responding to emails or calls from you.
<br/>Supplying you with information by email that you have opted-in to (you may unsubscribe or opt-out at any time by updating your preferences on our sites.
<br/>Analysing your use of LN2X and gathering feedback to enable us to continually improve LN2X and your user experience.
<br/>Updating you about logistical and operational information related to training courses.
<p>With your permission and/or where permitted by law, we may also use your personal data for marketing purposes, which may include contacting you by email, telephone,  whatsapp, text message and post with information, news, and offers on our services. You will not be sent any unlawful marketing or spam. We will always work to fully protect your rights and comply with our obligations under the GDPR and the Privacy and Electronic Communications (EC Directive) Regulations 2003, and you will always have the opportunity to opt-out.</p>
<p>Third Parties whose content appears on LN2X may use third-party Cookies, as detailed below in Part 14. Please refer to Part 14 for more information on controlling cookies. Please note that we do not control the activities of such third parties, nor the data that they collect and use themselves, and we advise you to check the privacy policies of any such third parties.</p>
<p>We use contextual rule based logic to send you notifications based on information you provide. We use email, website, IP and other information to automate certain decision-making processes in order to send you relevant information. If at any point you wish to query any action that we take on the basis of this or wish to request ‘human intervention’, the GDPR gives you the right to do so. Please contact us to find out more using the details in Part 15.</p>
<p>The following automated decision-making method(s) may be used:
<br/>Sending you notifications automatically based on your activity on our site.
<br/>Sending you relevant information regarding our services based on your activity, profile, interests, requests, interactions, and feedback results.
</p></p>
<h3>How Long Will You Keep My Personal Data?</h3>
<p>We will not keep your personal data for any longer than is necessary in light of the reason(s) for which it was first collected. Your personal data will therefore be kept for the following periods (or, where there is no fixed period, the following factors will be used to determine how long it is kept):</p>
<p>Financial Documents, Identity Documents, Signed Forms, and other sensitive information is kept for as long as we provide you with the service, this data is further retained encrypted for 18 months after the conclusion of the service for claims and legal purposes;
<br/>Contact Information is retained for as long as we provide you services for. You can update your profile preferences to decide on what information you want to receive from us, by default we only send updates that may be relevant to you. Contact details are deleted if we don't hear from you 6 months since your last contact with us.
<br/>All other information is stored only as long as we require such information in order to provide you with a service and according to legal requirements for data retention.</p>

<h3>How and Where Do You Store or Transfer My Personal Data?</h3>
<p>We may store some or all of your personal data in computer systems hosted in countries that are not part of the European Economic Area (the “EEA” consists of all EU member states, plus Norway, Iceland, and Liechtenstein). These are known as “third countries” and may not have data protection laws that are as strong as those in the UK and/or the EEA. This means that we will take additional steps in order to ensure that your personal data is treated just as safely and securely as it would be within the UK and under the GDPR as follows.</p>
<p>Where we transfer your data to a third party based in the US, this may be protected if they are part of the EU-US Privacy Shield. This requires that third party to provide data protection to standards similar to those in Europe. More information is available from the European Commission.</p>
<p>Please contact us using the details below in Part 15 for further information about the particular data protection mechanism used by us when transferring your personal data to a third country.</p>
<p>The security of your personal data is essential to us, and to protect your data, we take a number of important measures, including the following:</p>
<p>Internal Access based on a needs basis, only personnel which require access to your data to fulfill a service to you have access. Such access and permissions are reviewed constantly. All access to your data is logged and monitored and we have internal procedures in place to prosecute any breach of your privacy by our staff.
<br/>All access to our ICT infrastructure, whether on-premise or hosted externally, is secured via two-factor authentication and other standard industry best practices. Our ICT infrastructure is constantly reviewed by both automated and manual security testing methods to ensure your data is safe.
<br/>We subscribe to and comply with international security standards including PCI DSS, GDPR, ISO27001, ISO22301 and ISO9001 which require strict control over sensitive data and its underlying infrastructure.
</p>
<h3>Do You Share My Personal Data?</h3>
<p>We may sometimes contract with the following third parties to supply certain services. These may include payment processing, delivery, and marketing. In some cases, those third party systems may require access to some or all of your personal data that we hold. In such cases LN2X retains all control over your data and such parties should not have access to your information. We review all our third parties regularly to ensure that they implement strong security measures.</p>
<p>If any of your personal data is required by a third party, as described above, we will take steps to ensure that your personal data is handled safely, securely, and in accordance with your rights, our obligations, and the third party’s obligations under the law, as described above in Part 9.</p>
<p>In some limited circumstances, we may be legally required to share certain personal data, which might include yours, if we are involved in legal proceedings or complying with legal obligations, a court order, or the instructions of a government authority.</p>
<p>
<h3>How Can I Control My Personal Data?</h3>
<p>In addition to your rights under the GDPR, set out in Part 5, when you submit personal data via LN2X, you may be given options to restrict our use of your personal data. In particular, we aim to give you strong controls on our use of your data for direct marketing purposes (including the ability to opt-out of receiving emails from us which you may do by unsubscribing using the links provided in our emails or through account profile preferences where applicable.</p>
<p>You may also wish to sign up to one or more of the preference services operating in the UK: The Telephone Preference Service (“the TPS”), the Corporate Telephone Preference Service (“the CTPS”), and the Mailing Preference Service (“the MPS”). These may help to prevent you receiving unsolicited marketing. Please note, however, that these services will not prevent you from receiving marketing communications that you have consented to receiving.</p>
</p>
<h3>Can I Withhold Information?</h3>
<p>You may access certain areas of LN2X without providing any personal data at all. However, to use certain features and functions available on LN2X you may be required to submit or allow for the collection of certain data.You may restrict our use of Cookies. For more information, see Part 14.</p>

<h3>How Can I Access My Personal Data?</h3>

<p>If you want to know what personal data we have about you, you can ask us for details of that personal data and for a copy of it (where any such personal data is held). This is known as a “subject access request”.</p>
<p>All subject access requests should be made in writing and sent to the addresses shown in Part 15. </p>
<p>There is not normally any charge for a subject access request. If your request is ‘manifestly unfounded or excessive’ (for example, if you make repetitive requests) a fee may be charged to cover our administrative costs in responding.</p>
<p>We will respond to your subject access request within 14 days and, in any case, not more than one month of receiving it. Normally, we aim to provide a complete response, including a copy of your personal data within that time. In some cases, however, particularly if your request is more complex, more time may be required up to a maximum of three months from the date we receive your request. You will be kept fully informed of our progress.</p>


<h3>How Do You Use Cookies?</h3>
<p>LN2X may place and access certain first-party Cookies on your computer or device. First-party Cookies are those placed directly by us and are used only by us. We use Cookies to facilitate and improve your experience of LN2X and to provide and improve our services. By using LN2X you may also receive certain third-party Cookies on your computer or device. Third-party Cookies are those placed by websites, services, and/or parties other than us. Third-party Cookies are used on LN2X for communication(chat), and website analytics. In addition, LN2X uses analytics services, which also use Cookies. Website analytics refers to a set of tools used to collect and analyse usage statistics, enabling us to better understand how people use LN2X. </p>
<p>All Cookies used by and on LN2X are used in accordance with current Cookie Law.</p>
<p>Before Cookies are placed on your computer or device, you will be shown a pop-up requesting your consent to set those Cookies. By giving your consent to the placing of Cookies you are enabling us to provide the best possible experience and service to you. You may, if you wish, deny consent to the placing of Cookies; however certain features of LN2X may not function fully or as intended. </p>
<p>In addition to the controls that we provide, you can choose to enable or disable Cookies in your internet browser. Most internet browsers also enable you to choose whether you wish to disable all Cookies or only third-party Cookies. By default, most internet browsers accept Cookies, but this can be changed. For further details, please consult the help menu in your internet browser or the documentation that came with your device.</p>
<p>You can choose to delete Cookies on your computer or device at any time, however you may lose any information that enables you to access LN2X more quickly and efficiently including, but not limited to, login and personalisation settings.</p>
<p>It is recommended that you keep your internet browser and operating system up-to-date and that you consult the help and guidance provided by the developer of your internet browser and manufacturer of your computer or device if you are unsure about adjusting your privacy settings.</p>

<h3>How Do I Contact You?</h3>
<p>To contact us about anything to do with your personal data and data protection, including to make a subject access request, please use the following details:
<br/>Email address: dpo@ln2x.com
<br/>Telephone number: +44 2033229095
</p>

<h3>Changes to this Privacy Policy</h3>
<p>We may change this Privacy Notice from time to time. This may be necessary, for example, if the law changes, or if we change our business in a way that affects personal data protection.</p>
<p>Any changes will be immediately posted on LN2X and you will be deemed to have accepted the terms of the Privacy Policy on your first use of LN2X following the alterations. We recommend that you check this page regularly to keep up-to-date.</p>
  
    </div>
  </div>
</section>
  </Layout>
)

export default PrivacyPage;
