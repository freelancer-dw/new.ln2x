import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const ControlPage = () => (
  <Layout>
    <SEO title="GRC" />
    <section className="fdb-block" data-block-type="features" data-id="24" draggable="false">
      <div className="container">
        <div className="row text-center">
          <div className="col-12">
            <h1>{lex} GRC</h1>
            <p className="lead"> {lex} GRC provides the building blocks to create a lightweight extensible control framework which addresses infosec and compliance challenges efficiently and provides enhance visibility, metrics and implementation of governance, risk and compliance programs. If you would like to get involved just contact us!</p>
          </div>
        </div>
      </div>
    </section>

    <section className="fdb-block" data-block-type="call_to_action" data-id="14" draggable="true">
      <div className="container">
        <div className="row align-items-center pt-2 pt-lg-1">
          <div className="col-12 col-md-8 col-lg-7">
            <h2>Really?</h2>
            <h3>Still trying to manage risk assesments, audits, reports, evidence with Spreadsheets? Trying to import, export, correlate, get metrics, automate notifications, keep track of what is actually happening? sound familiar? well we decided to build it ourselves!</h3>
          </div>
          <div className="col-8 col-md-5 m-auto m-md-0 ml-md-auto pt-lg-1">
            <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_collecting_fjjl.png"/>
          </div>
        </div>
        </div>
    </section>
    <section className="fdb-block" data-block-type="features" data-id="24" draggable="false">
      <div className="container">
        <div className="row text-center">
          <div className="col-12">
            <h2>Our solution: {lex}</h2>
            <p className="lead" align="center"> After years of listening to thousands of information security professionals talk about the same pain points we realised something has to be done. The great majority of data breaches are down to human intervention whether direct or indirect, intentional or unintentional. Effectively we need a system that enables you to log/document/measure standards, frameworks, policies, processes, procedures, tasks, risks, assets, metrics, performance, KPI's, controls. The system should be available and be accesible to anyone and easy to understand.</p>
          </div>
        </div>
      </div>
    </section>

    <section className="fdb-block" data-block-type="features" data-id="24" draggable="false">
      <div className="container">
        <div className="row text-left mt-5">
          <div className="col-12 col-sm-6 col-lg-3">
            <div className="row">
              <div className="col-3" data-aos="fade-up" data-aos-delay="600">
                <img alt="image" className="img-fluid" src="/assets/icons/layers.svg"/>
              </div>
              <div className="col-9" data-aos="fade-down" data-aos-delay="800">
                <h4>Interface</h4>
                <p>Lightning Fast UI build with the latest tech. Can you believe it?</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-sm-0" data-aos="fade-right" data-aos-delay="600">
            <div className="row">
              <div className="col-3">
                <img alt="image" className="img-fluid" src="/assets/icons/layout.svg"/>
              </div>
              <div className="col-9" data-aos="fade-down" data-aos-delay="800">
                <h4>Policies & Procedures</h4>
                <p>Turn policies into action, measure conformance, track dependencies. Yes really.</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0 " data-aos="fade-up" data-aos-delay="600">
            <div className="row">
              <div className="col-3">
                <img alt="image" className="img-fluid" src="/assets/icons/life-buoy.svg"/>
              </div>
              <div className="col-9" data-aos="fade-down" data-aos-delay="800">
                <h4>Risk Assesment</h4>
                <p>Easily Manage Risks, link anything (Assets, Controls, Requirements...)</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0"data-aos="fade-right" data-aos-delay="600">
            <div className="row">
              <div className="col-3" data-aos="fade-down" data-aos-delay="800">
                <img alt="image" className="img-fluid" src="/assets/icons/map-pin.svg"/>
              </div>
              <div className="col-9" data-aos="fade-right" data-aos-delay="1200">
                <h4>Project Management</h4>
                <p>Assign, review, approve, schedule, plan, track, single and recurring tasks. <br/>No more losing track.</p>
              </div>
            </div>
          </div>
        </div>

        <div className="row text-left pt-3 pt-lg-5">
          <div className="col-12 col-sm-6 col-lg-3 " data-aos="fade-up" data-aos-delay="600">
            <div className="row">
              <div className="col-3" data-aos="fade-down" data-aos-delay="200">
                <img alt="image" className="img-fluid" src="/assets/icons/monitor.svg"/>
              </div>
              <div className="col-9" data-aos="fade-up" data-aos-delay="500">
                <h4>Risk Management</h4>
                <p>Executive and Detailed Reporting anytime with in depth analytics. No more Spreadsheets.</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-sm-0"data-aos="fade-right" data-aos-delay="600">
            <div className="row">
              <div className="col-3" data-aos="fade-up" data-aos-delay="500">
                <img alt="image" className="img-fluid" src="/assets/icons/package.svg"/>
              </div>
              <div className="col-9" data-aos="fade-left" data-aos-delay="500">
                <h4>Training & Communication</h4>
                <p>In-built KPIs, CSFs, L&D Metrics. Staff development analytics without the pain.</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0 " data-aos="fade-up" data-aos-delay="600">
            <div className="row">
              <div className="col-3">
                <img alt="image" className="img-fluid" src="/assets/icons/cloud.svg"/>
              </div>
              <div className="col-9" data-aos="fade-up" data-aos-delay="900">
                <h4>Control Mapping</h4>
                <p>PCI DSS, ISO27001, GDPR, NIST, CIS... Manage multiple standards with one consolidated control framework.</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-lg-3 pt-3 pt-lg-0">
            <div className="row">
              <div className="col-3" data-aos="fade-up" data-aos-delay="500">
                <img alt="image" className="img-fluid" src="/assets/icons/gift.svg"/>
              </div>
              <div className="col-9" data-aos="fade-down" data-aos-delay="500">
                <h4>Compliance?</h4>
                <p>Automated reporting with in-depth drill down, always up to date</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="fdb-block" data-block-type="features" data-id="24" draggable="false">
      <div className="container">
        <div className="row text-center">
          <div className="col-12">
            <h1>Sign up for an early release demo</h1>
            <p>We're building something great, sign up here to schedule a demo</p>
            <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="https://calendly.com/ln2x" target="_blank">Schedule a Demo</a></p>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default ControlPage
