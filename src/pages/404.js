import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1 align="center">PAGE NOT FOUND</h1>
    <p align="center">You just hit a route that doesn&#39;t exist... sorry. We must have moved this page somewhere</p>
  </Layout>
)

export default NotFoundPage